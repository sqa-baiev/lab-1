package edu.chmnu.baiev.lab_1;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class AppStartupRunner implements ApplicationRunner {
    private final BrakePadTestService brakePadTestService;
    private final FuelFilterTestService fuelFilterTestService;
    private final SparkPlugTestService sparkPlugTestService;

    @Override
    public void run(ApplicationArguments args) {
        brakePadTestService.runTest();
        fuelFilterTestService.runTest();
        sparkPlugTestService.runTest();
    }
}