package edu.chmnu.baiev.lab_1.mapper;

import edu.chmnu.baiev.lab_1.db.entity.BrakePad;
import edu.chmnu.baiev.lab_1.db.entity.FuelFilter;
import edu.chmnu.baiev.lab_1.dto.FuelFilterDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

@Service
@Slf4j
public class FuelFilterMapper {

    private final Validator validator;

    public FuelFilterMapper(Validator validator) {
        this.validator = validator;
    }

    public FuelFilterDto map(FuelFilter entity) {
        Set<ConstraintViolation<FuelFilter>> validate = validator.validate(entity);
        if (!validate.isEmpty()) {
            for (ConstraintViolation<FuelFilter> violation : validate) {
                log.error("Error characteristic " + violation.getPropertyPath().toString());
            }
            return null;
        }
        return FuelFilterDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .filterType(entity.getFilterType())
                .fuelType(entity.getFuelType())
                .outerDiameter(Integer.valueOf(entity.getOuterDiameter()))
                .innerDiameter(Integer.valueOf(entity.getInnerDiameter()))
                .height(Integer.valueOf(entity.getHeight()))
                .build();
    }
}
