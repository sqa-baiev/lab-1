package edu.chmnu.baiev.lab_1.mapper;

import edu.chmnu.baiev.lab_1.db.entity.BrakePad;
import edu.chmnu.baiev.lab_1.dto.BrakePadDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

@Service
@Slf4j
public class BrakePadMapper {

    private final Validator validator;

    public BrakePadMapper(Validator validator) {
        this.validator = validator;
    }

    public BrakePadDto map(BrakePad entity) {
        Set<ConstraintViolation<BrakePad>> validate = validator.validate(entity);
        if (!validate.isEmpty()) {
            for (ConstraintViolation<BrakePad> violation : validate) {
                log.error("Error characteristic " + violation.getPropertyPath().toString());
            }
            return null;
        }
        return BrakePadDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .width(Integer.valueOf(entity.getWidth()))
                .height(Integer.valueOf(entity.getHeight()))
                .depth(Integer.valueOf(entity.getDepth()))
                .type(entity.getType())
                .wearGauge(entity.isWearGauge())
                .wearGaugeAmount(Integer.valueOf(entity.getWearGaugeAmount()))
                .weight(Double.valueOf(entity.getWeight()))
                .build();
    }
}
