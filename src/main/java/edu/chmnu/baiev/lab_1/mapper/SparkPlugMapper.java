package edu.chmnu.baiev.lab_1.mapper;

import edu.chmnu.baiev.lab_1.db.entity.SparkPlug;
import edu.chmnu.baiev.lab_1.dto.SparkPlugDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

@Service
@Slf4j
public class SparkPlugMapper {

    private final Validator validator;

    public SparkPlugMapper(Validator validator) {
        this.validator = validator;
    }

    public SparkPlugDto map(SparkPlug entity) {
        Set<ConstraintViolation<SparkPlug>> validate = validator.validate(entity);
        if (!validate.isEmpty()) {
            for (ConstraintViolation<SparkPlug> violation : validate) {
                log.error("Error characteristic " + violation.getPropertyPath().toString());
            }
            return null;
        }
        return SparkPlugDto.builder()
                .name(entity.getName())
                .electrodeDistance(Integer.valueOf(entity.getElectrodeDistance()))
                .connection(entity.getConnection())
                .threadSize(entity.getThreadSize())
                .spannerWidth(Integer.valueOf(entity.getSpannerWidth()))
                .type(entity.getType())
                .carvingLength(Integer.valueOf(entity.getCarvingLength()))
                .maxResistance(Integer.valueOf(entity.getMaxResistance()))
                .build();
    }
}
