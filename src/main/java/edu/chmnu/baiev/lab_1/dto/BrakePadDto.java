package edu.chmnu.baiev.lab_1.dto;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BrakePadDto {

    private Long id;

    private String name;

    private Integer width;

    private Integer height;

    private Integer depth;

    private String type;

    private boolean wearGauge;

    private Integer wearGaugeAmount;

    private Double weight;
}
