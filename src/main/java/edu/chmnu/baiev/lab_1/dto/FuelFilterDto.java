package edu.chmnu.baiev.lab_1.dto;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FuelFilterDto {

    private Long id;

    private String name;

    private String filterType;

    private String fuelType;

    private Integer outerDiameter;

    private Integer innerDiameter;

    private Integer height;
}
