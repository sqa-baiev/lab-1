package edu.chmnu.baiev.lab_1.dto;
import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SparkPlugDto {

    private Long id;

    private String name;

    private Integer electrodeDistance;

    private String connection;

    private String threadSize;

    private Integer spannerWidth;

    private String type;

    private Integer carvingLength;

    private Integer maxResistance;
}
