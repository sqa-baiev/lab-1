package edu.chmnu.baiev.lab_1;

import edu.chmnu.baiev.lab_1.db.entity.FuelFilter;
import edu.chmnu.baiev.lab_1.db.entity.SparkPlug;
import edu.chmnu.baiev.lab_1.db.repository.SparkPlugRepository;
import edu.chmnu.baiev.lab_1.mapper.SparkPlugMapper;
import org.springframework.stereotype.Service;

@Service
public class SparkPlugTestService {

    private final SparkPlugRepository sparkPlugRepository;
    private final SparkPlugMapper sparkPlugMapper;

    public SparkPlugTestService(SparkPlugRepository sparkPlugRepository,
                                SparkPlugMapper sparkPlugMapper) {
        this.sparkPlugRepository = sparkPlugRepository;
        this.sparkPlugMapper = sparkPlugMapper;
    }

    public void runTest() {
        Iterable<SparkPlug> all = sparkPlugRepository.findAll();
        for (SparkPlug plug : all) {
            sparkPlugMapper.map(plug);
            System.out.println(plug.toString());
        }
    }
}
