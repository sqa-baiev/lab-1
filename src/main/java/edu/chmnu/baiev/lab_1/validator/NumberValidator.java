package edu.chmnu.baiev.lab_1.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class NumberValidator implements ConstraintValidator<Number, String> {
    private final Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

    @Override
    public void initialize(Number constraintAnnotation) {
        
    }

    @Override
    public boolean isValid(String field, ConstraintValidatorContext constraintValidatorContext) {
        if (field == null) {
            return false;
        }
        return pattern.matcher(field).matches();
    }
}
