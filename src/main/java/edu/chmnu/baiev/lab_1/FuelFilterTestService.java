package edu.chmnu.baiev.lab_1;

import edu.chmnu.baiev.lab_1.db.entity.BrakePad;
import edu.chmnu.baiev.lab_1.db.entity.FuelFilter;
import edu.chmnu.baiev.lab_1.db.repository.FuelFilterRepository;
import edu.chmnu.baiev.lab_1.mapper.FuelFilterMapper;
import org.springframework.stereotype.Service;

@Service
public class FuelFilterTestService {

    private final FuelFilterRepository fuelFilterRepository;
    private final FuelFilterMapper fuelFilterMapper;

    public FuelFilterTestService(FuelFilterRepository fuelFilterRepository,
                                 FuelFilterMapper fuelFilterMapper) {
        this.fuelFilterMapper = fuelFilterMapper;
        this.fuelFilterRepository = fuelFilterRepository;
    }

    public void runTest() {
        Iterable<FuelFilter> all = fuelFilterRepository.findAll();
        for (FuelFilter filter : all) {
            fuelFilterMapper.map(filter);
            System.out.println(filter.toString());
        }
    }
}
