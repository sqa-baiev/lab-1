package edu.chmnu.baiev.lab_1.db.entity;
import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "spark_plug")
public class SparkPlug {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "electrode_distance")
    private String electrodeDistance;

    @Column(name = "connection")
    private String connection;

    @Column(name = "thread_size")
    private String threadSize;

    @Column(name = "spanner_width")
    private String spannerWidth;

    @Column(name = "type")
    private String type;

    @Column(name = "carving_length")
    private String carvingLength;

    @Column(name = "max_resistance")
    private String maxResistance;
}
