package edu.chmnu.baiev.lab_1.db.repository;

import edu.chmnu.baiev.lab_1.db.entity.BrakePad;
import org.springframework.data.repository.CrudRepository;

public interface BrakePadRepository extends CrudRepository<BrakePad, Long> {
}
