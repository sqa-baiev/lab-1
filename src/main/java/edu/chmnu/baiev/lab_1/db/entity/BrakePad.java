package edu.chmnu.baiev.lab_1.db.entity;

import edu.chmnu.baiev.lab_1.validator.Number;
import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "brake_pad")
public class BrakePad {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "width")
    @Number
    private String width;

    @Column(name = "height")
    @Number
    private String height;

    @Column(name = "depth")
    @Number
    private String depth;

    @Column(name = "type")
    private String type;

    @Column(name = "wear_gauge")
    private boolean wearGauge;

    @Column(name = "wear_gauge_amount")
    @Number
    private String wearGaugeAmount;

    @Column(name = "weight")
    @Number
    private String weight;
}
