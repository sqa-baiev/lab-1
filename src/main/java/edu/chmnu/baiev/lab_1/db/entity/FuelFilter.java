package edu.chmnu.baiev.lab_1.db.entity;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "fuel_filter")
public class FuelFilter {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "filter_type")
    private String filterType;

    @Column(name = "fuel_type")
    private String fuelType;

    @Column(name = "outer_diameter")
    private String outerDiameter;

    @Column(name = "inner_diameter")
    private String innerDiameter;

    @Column(name = "height")
    private String height;
}
