package edu.chmnu.baiev.lab_1.db.repository;

import edu.chmnu.baiev.lab_1.db.entity.FuelFilter;
import org.springframework.data.repository.CrudRepository;

public interface FuelFilterRepository extends CrudRepository<FuelFilter, Long> {
}
