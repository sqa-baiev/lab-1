package edu.chmnu.baiev.lab_1.db.repository;

import edu.chmnu.baiev.lab_1.db.entity.SparkPlug;
import org.springframework.data.repository.CrudRepository;

public interface SparkPlugRepository extends CrudRepository<SparkPlug, Long> {
}
