package edu.chmnu.baiev.lab_1;

import edu.chmnu.baiev.lab_1.db.entity.BrakePad;
import edu.chmnu.baiev.lab_1.db.repository.BrakePadRepository;
import edu.chmnu.baiev.lab_1.mapper.BrakePadMapper;
import org.springframework.stereotype.Service;

@Service
public class BrakePadTestService {

    private final BrakePadRepository brakePadRepository;
    private final BrakePadMapper brakePadMapper;

    public BrakePadTestService(BrakePadRepository brakePadRepository,
                               BrakePadMapper brakePadMapper) {
        this.brakePadRepository = brakePadRepository;
        this.brakePadMapper = brakePadMapper;
    }

    public void runTest() {
        Iterable<BrakePad> all = brakePadRepository.findAll();
        for (BrakePad pad : all) {
            brakePadMapper.map(pad);
            System.out.println(pad.toString());
        }
    }
}
