DROP TABLE IF EXISTS brake_pad;

CREATE TABLE brake_pad
(
    id                INT AUTO_INCREMENT PRIMARY KEY,
    name              varchar(250),
    width             varchar(250),
    height            varchar(250),
    depth             varchar(250),
    type              varchar(250),
    wear_gauge        BOOLEAN default false,
    wear_gauge_amount varchar(250),
    weight            varchar(250)
);

INSERT INTO brake_pad(name, width, height, depth, type, wear_gauge, wear_gauge_amount, weight)
VALUES ('test', '1', '5', '1', 'type', true, '5', '6'), ('test', '1', '5', 'fff', 'type', true, '5', '6')
, ('test', '1', 'ddd', '1', 'type', true, 'fff', '6');
